<?php

/**
 * @file
 * Download gitlab dependencies via drush framework.
 */

/**
 * Implements hook_drush_command().
 */
function gitlab_release_log_drush_command() {
  $items['gitlab-release-create'] = array(
    'callback' => 'drush_gitlab_release_log_create_release',
    'description' => 'Create release in GitLab.',
    'arguments' => array(
      'release_name' => 'Name of the release to create.',
      'release_message' => 'Description of the release like commit message.',
    ),
    'aliases' => array('gl-r-c'),
  );
  return $items;
}

/**
 * Command callback. Create a release.
 */
function drush_gitlab_release_log_create_release($release_name = "", $release_message = "") {
  gitlab_release_log_create_release($release_name, $release_message);
}

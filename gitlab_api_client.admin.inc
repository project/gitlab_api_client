<?php

function gitlab_api_client_settings_form($form, &$form_state) {
  $form['gitlab_url'] = array(
    '#title' => t('URL'),
    '#type' => 'textfield',
    '#default_value' => variable_get('gitlab_url', 'https://gitlab.com/api/v3/'),
    '#description' => t('Example: https://gitlab.com/api/v3/'),
    '#required' => TRUE,
  );

  $form['gitlab_token'] = array(
    '#title' => t('Private token'),
    '#type' => 'textfield',
    '#default_value' => variable_get('gitlab_token', ''),
    '#description' => t('You can get your private token from https://gitlab.com/profile/account'),
    '#required' => TRUE,
  );

  $form['gitlab_project'] = array(
      '#title' => t('Project'),
      '#type' => 'textfield',
      '#default_value' => variable_get('gitlab_project', ''),
      '#description' => t('Project, identified by NAMESPACE/PROJECT_NAME. Example: In https://gitlab.com/gitlab-org/gitlab-ce you would be typing gitlab-org/gitlab-ce'),
  );

  $form['gitlab_build_id'] = array(
      '#title' => t('Build ID'),
      '#type' => 'textfield',
      '#default_value' => variable_get('gitlab_build_id', ''),
      '#description' => t('Build ID of the project.'),
  );

  $form = system_settings_form($form);
  return $form;
}
